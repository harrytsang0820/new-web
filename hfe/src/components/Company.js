import React from "react";
import Company1 from "../assets/1.jpg";
import Company2 from "../assets/2.jpg";
import Company3 from "../assets/5.jpg";
import Company4 from "../assets/8.jpg";
import Company5 from "../assets/10.jpg";
import Company6 from "../assets/12.jpg";
import CompanyData from "./CompanyData";

const Company = () => {
  return (
    <>
      <div className="destination">
        <h1>Jingliving INC</h1>
        <p>A New Way to Purchase Your Sauna Room.</p>
        <CompanyData
          className="first-des"
          heading="Our Goal"
          text="Founded in 2019, JingLiving Inc. stands as a pioneering e-commerce 
          platform in the U.S., revolutionizing the way premium sauna rooms are purchased. 
          Specializing in mid to high-end sauna creations, every piece from JingLiving is exquisitely 
          crafted using either African Okoume or Canadian Hemlock wood, ensuring not just luxury but 
          unparalleled quality for its discerning clientele"
          img1={Company1}
          img2={Company2}
        />

        <CompanyData
          className="first-des-reverse"
          heading="Our Platform"
          text="JingLiving Inc. is a renowned name in the e-commerce landscape with its esteemed presence on major 
          platforms like Amazon, Wayfair, and Walmart. With years of expertise in managing online storefronts, 
          we have not only carved a niche in the digital marketplace but have also cultivated trust among our global 
          clientele. Beyond our robust online operations, our production technology for sauna rooms is at the forefront 
          of global innovation, ensuring that every JingLiving product is a testament to cutting-edge design and 
          superior craftsmanship."
          img1={Company3}
          img2={Company4}
        />
        <CompanyData
          className="first-des"
          heading="Our Factory"
          text="Our machinery performs with unparalleled precision. It can execute high-precision cutting, 
          ensuring that every piece is cut to exact measurements. Furthermore, our equipment is adept at accurate punching, ensuring 
          that every hole or notch is right where it needs to be. One of the standout features of our manufacturing setup is its ability to save materials, 
          reducing waste and ensuring an eco-friendly production process.
          Our expansive facility spreads across 3 industrial zones. These zones have the capacity to produce 5 different Sauna products simultaneously, underlining our ability to handle diverse orders with ease. The total production area sprawls across a vast 60,000 square meters, making us one of the largest in the industry."
          img1={Company5}
          img2={Company6}
        />
      </div>
    </>
  );
};

export default Company;
