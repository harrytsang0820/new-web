import Navbar from "../components/Navbar";
import HomeHero from "../components/HomeHero";
import Company from "../components/Company";
import Footer from "../components/Footer";
import Shop from "../components/Shop";
import MainVideo from "../assets/Home.mp4";

function Home() {
  return (
    <>
      <Navbar />
      <HomeHero
        cName="hero"
        video={ MainVideo } 
        title="Jingliving / Hongyuan Saunas"
        text="Steam Your Dreams: Experience Tranquility."
        btnClass="show"
        buttonText="Wayfair"
        url="/https://www.wayfair.com/brand/bnd/royal-saunas-hongyuan-b55239.html"
        secondBtnClass="show"  
        secondButtonText="Walmart" 
        secondUrl="/https://www.walmart.com/ip/Jingliving-Single-Person-EXTENDABLE-Indoor-FAR-Infrared-Heating-Sauna-Bluetooth-Compatible/2134271801?from=/search"  
      />
      <Company />
      <Shop />
      <Footer />
    </>
  );
}

export default Home;
